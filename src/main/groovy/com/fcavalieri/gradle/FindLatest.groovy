package com.fcavalieri.gradle

import groovy.json.JsonSlurper
import org.apache.groovy.json.internal.LazyMap
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.credentials.HttpHeaderCredentials
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Files

class FindLatestPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.extensions.create("findLatest", FindLatestExtension, project)
    }
}

class DockerImage {
    String name
    String path
    String location
    public DockerImage(String name, String path, String location) {
        this.name = name
        this.path = path
        this.location = location
    }
}

class FindLatestExtension {
    static final Logger logger = LoggerFactory.getLogger("FindLatestPlugin")
    static final String defaultGitlabEndpoint = "https://gitlab.com";
    Project project = null

    FindLatestExtension(Project project) {
        this.project = project
    }

    DockerImage findLatestImage(String gitlabEndpoint, String projectId, String imageName) {
        def creds = new GitLabCredentialsExtension(project)
        def mergeCommitsUrl = new URL(gitlabEndpoint + "/api/v4/projects/" + projectId + "/repository/commits?first_parent=true")
        def mergeCommitsReq = mergeCommitsUrl.openConnection()
        mergeCommitsReq.setDoInput(true)
        mergeCommitsReq.setRequestMethod('GET')
        creds.apply(mergeCommitsReq)
        def mergeCommitsRC = mergeCommitsReq.getResponseCode();
        if (!mergeCommitsRC.equals(200)) {
            throw new GradleException("Cannot list project merge commits")
        }
        def mergeCommitsList = new JsonSlurper().parseText( mergeCommitsReq.getInputStream().text )
        def mergeCommitIdsList = mergeCommitsList.collect { value -> value.id }

        def registryRepositoriesUrl = new URL(gitlabEndpoint + "/api/v4/projects/" + projectId + "/registry/repositories")
        def registryRepositoriesReq = registryRepositoriesUrl.openConnection()
        registryRepositoriesReq.setDoInput(true)
        registryRepositoriesReq.setRequestMethod('GET')
        creds.apply(registryRepositoriesReq)
        def registryRepositoriesRC = registryRepositoriesReq.getResponseCode();
        if (!registryRepositoriesRC.equals(200)) {
            throw new GradleException("Cannot list project merge commits")
        }
        def registryRepositoriesList = new JsonSlurper().parseText( registryRepositoriesReq.getInputStream().text )
        def matchingRepository = registryRepositoriesList.find{ it.name == imageName }
        if (matchingRepository == null) {
            throw new RuntimeException("No matching GitLab repository exists")
        }
        def matchingRepositoryId = matchingRepository.id


        def tagsUrl = new URL(gitlabEndpoint + "/api/v4/projects/" + projectId + "/registry/repositories/" + matchingRepositoryId + "/tags")
        def tagsReq = tagsUrl.openConnection()
        tagsReq.setDoInput(true)
        tagsReq.setRequestMethod('GET')
        creds.apply(tagsReq)
        def tagsRC = tagsReq.getResponseCode();
        if (!tagsRC.equals(200)) {
            throw new GradleException("Cannot list project merge commits")
        }
        def tagsList = new JsonSlurper().parseText( tagsReq.getInputStream().text )

        def matchingTag = null
        for (String commitId: mergeCommitIdsList) {
            matchingTag = tagsList.find{ commitId.startsWith(((String)(it.name)).split("-").last()) }
            if (matchingTag == null) {
                logger.error( "No image found for commit id " + commitId)
            } else {
                break;
            }
        }
        if (matchingTag == null) {
            throw new RuntimeException("No image found")
        }
        return new DockerImage(matchingTag.name, matchingTag.path, matchingTag.location)
    }

    String findLatestImagePath(String gitlabEndpoint, String projectId, String imageName) {
        return findLatestImage(gitlabEndpoint, projectId, imageName).path
    }

    String findLatestImageLocation(String gitlabEndpoint, String projectId, String imageName) {
        return findLatestImage(gitlabEndpoint, projectId, imageName).location
    }

    DockerImage findLatestImage(String projectId, String imageName) {
        return findLatestImage(defaultGitlabEndpoint, projectId, imageName)
    }

    String findLatestImagePath(String projectId, String imageName) {
        return findLatestImage(defaultGitlabEndpoint, projectId, imageName).path
    }

    String findLatestImageLocation(String projectId, String imageName) {
        return findLatestImage(defaultGitlabEndpoint, projectId, imageName).location
    }
}
