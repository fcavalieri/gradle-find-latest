/*
 * This Groovy source file was generated by the Gradle 'init' task.
 */
package com.fcavalieri.gradle

import com.fuseanalytics.gradle.s3.S3Upload
import org.gradle.api.tasks.bundling.Zip
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification

class FindLatestPluginTest extends Specification {
    def "plugin registers task"() {
        given:
        def project = ProjectBuilder.builder().build()
        when:
        project.plugins.apply("com.fcavalieri.gradle.findlatest")

        then:
        project.extensions.findByName("findLatest") != null
    }
}
