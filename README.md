# Gradle Gitlab Credentials

## Usage

settings.gradle
```groovy
pluginManagement {
    repositories {
        maven { url "https://gitlab.com/api/v4/projects/35261469/packages/maven" }
        gradlePluginPortal()
    }
}
```

build.gradle
```groovy
plugins {
    id "com.fcavalieri.gradle.findlatest" version 'X.X.X'
}
```

Usage:

```groovy
task mytask {
    doLast {
        findlatest.findLatestImage("projectId", "imagePath")
    }
}
```

```groovy
test {
    environment "CELLSTORE_SERVER_IMAGE", findlatest.findLatestImage("projectId", "imagePath")
}
```